package com.example.portraitlandscapepresentk2023

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity(),AdapterView.OnItemSelectedListener {
    private lateinit var adapter: ArrayAdapter<CharSequence>
    private lateinit var images: Array<Int>
    private lateinit var iv: ImageView
    private var imgIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        images = arrayOf(R.drawable.car1, R.drawable.car2, R.drawable.car3)
        iv = findViewById(R.id.picture)

        updateImage()

        //adapter = ArrayAdapter()
        adapter = ArrayAdapter.createFromResource(this, R.array.pictures, R.layout.item)
        val spinner:Spinner? = findViewById(R.id.pictures_list)

        spinner?.adapter = adapter
        spinner?.onItemSelectedListener = this
    }

    fun onChangePictureClick(v: View) {
        imgIndex = (imgIndex + 1) % images.size
        updateImage()
    }

    private fun updateImage() {
        iv.setImageResource(images[imgIndex])
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        Toast.makeText(this, "выбран элемент $position", Toast.LENGTH_SHORT ).show()
        imgIndex = position
        updateImage()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        Toast.makeText(this, "не выбран элемент", Toast.LENGTH_SHORT ).show()
    }
}